// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

var outputHewan = [];

var insertedHewan;

for (var i = 0, ii = daftarHewan.length ; i < ii ; i++){
    insertedHewan = false;
    for (var j = 0, jj = outputHewan.length ; j < jj ; j++){
      if (daftarHewan[i] < outputHewan[j]){
        insertedHewan = true;
        outputHewan.splice(j, 0, daftarHewan[i]);
        break;
      }
    }
    
    if (!insertedHewan)
      outputHewan.push(daftarHewan[i])
  }
  
  console.log(outputHewan);

// soal 2
function introduce(){
  return "Nama saya " + data.name + " umur saya " + data.age + " tahun, alamat saya di " + data.address +", dan saya punya hobby yaitu " + data.hobby;
}

var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data);
console.log(perkenalan);

// soal 3
function hitung_huruf_vokal(str) {
  return str.replace(/[^aeiou]/gi, "").length;
};

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// soal 4
function hitung(int, angka = -2){
  return angka = angka + (int * 2);
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8