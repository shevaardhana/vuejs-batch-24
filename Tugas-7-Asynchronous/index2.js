var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
const time = 10000

// readBooksPromise(time, books[0])
//     .then(sisaWaktu0 => {
//         readBooksPromise(sisaWaktu0, books[1])
//         .then(sisaWaktu1 => {
//             readBooksPromise(sisaWaktu1, books[2])
//             .then(sisaWaktu2 => {
//                 readBooksPromise(sisaWaktu2, books[3])
//                 .then(sisaWaktu3 => {
//                     return
//                 })
//             })
//         })
//     })

//     .catch(err => console.log(err))

const execute = async() =>{
    const sisaWaktu0 = await readBooksPromise(time, books[0])
    const sisaWaktu1 = await readBooksPromise(time, books[1])
    const sisaWaktu2 = await readBooksPromise(time, books[2])
    const sisaWaktu3 = await readBooksPromise(time, books[3])
    
    return
} 

execute()