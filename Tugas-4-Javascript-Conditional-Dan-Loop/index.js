// soal 1
var nilai = 83;

if(nilai >= 85){
    console.log("A");
}
else if(nilai >= 75 && nilai < 85){
    console.log("B");
}
else if(nilai >= 65 && nilai < 75){
    console.log("C");
}
else if(nilai >= 55 && nilai < 65){
    console.log("D");
}
else{
    console.log("E");
}

// soal 2
var tanggal = 21;
var bulan = 12;
var tahun = 2000;

switch(bulan){
    case 12:
        console.log(tanggal+" Desember "+tahun);
}

// soal 3
function segitiga(n) {
    var hasil = '';
    for (var i = 0; i < n; i++) {
        for (var j = 0; j <= i; j++) {
            hasil += '* ';
        }
        hasil += '\n';
    }
    return hasil;
}
console.log(segitiga(7));

// soal 4
var jawaban2 = "";
var m = 10;

for(x = 1; x <= m; x++ ){
    if ((x % 3) == 0){
        jawaban2 += [x] + " -" + " " + "I love VueJS" + "\n" + "===" + "\n";
    }
    else if((x % 2) == 0){
        jawaban2 += [x] + " -" + " " + "I love Javascript" + "\n";
    }
    else{
        jawaban2 += [x] + " -" + " " + "I love programming" + "\n";
    }
}
console.log(jawaban2);