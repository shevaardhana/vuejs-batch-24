// soal 1
const luas = (panjang, lebar) => {
    return panjang * lebar;
}

const keliling = (panjang, lebar) =>{
    return 2 * (panjang + lebar);
}

console.log(luas(5, 2));
console.log(keliling(4, 2));

// soal 2
const newFunction = (firstName, lastName) =>{
    return {
        firstName,
        lastName,
        fullName: function() {
            console.log(`${firstName} ${lastName}`)
        }
    } 
}
   
//Driver Code 
newFunction("William", "Imoh").fullName();

// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}

const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combinedArray = [... west, ...east]
const combined = west.concat(east)

console.log(combined)

// soal 5
const planet = "earth" 
const view = "glass" 

var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet

var after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log(after);